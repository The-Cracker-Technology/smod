rm -rf /opt/ANDRAX/smod

source /opt/ANDRAX/PYENV/python2/bin/activate

/opt/ANDRAX/PYENV/python2/bin/pip2 install scapy==2.4.4

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "SCAPY 2.4.4 install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf $(pwd) /opt/ANDRAX/smod/

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
